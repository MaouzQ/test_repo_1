# Test mean stack project #

# Environment installation instruction #

### Prerequest: ###

* [Git](https://git-scm.com/downloads)

        git --version

    > Note:  
    When you use source tree with any IDE (in our case, I recommend Visual Studio Code, which is friendly to javascript), make sure they are both linked to the same git on your machine.  
    When you installing git on windows, choose the option which will let you use git command on both Command Prompt and git_bash

## Node.js (version: v6.**)

Download source: [node.js](https://nodejs.org/en/download/)


### Windows ###

Node.js install:

1. Download the Windows installer from the [source](https://nodejs.org/en/download/).

2. Run the installer (the .msi file you downloaded in the previous step.)

3. Follow the prompts in the installer (Accept the license agreement, click the NEXT button a bunch of times and accept the default installation settings). 

    ![installer.png](https://bitbucket.org/repo/ekryobr/images/1167897640-installer.png)

4. Restart your computer. You won’t be able to run Node.js® until you restart your computer.


### Mac ###

System Prerequest:

1. Xcode
2. HomeBrew

        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Node.js install:

1. Open the Terminal app and type:

        brew install node

2. Sit back and wait. Homebrew downloads some files and installs them. And that’s it.


### Test Node&&npm ###

* Type following command in powershell(or cmd, git-bash, terminal) to test you got Node.js correct

    Node test:

        node -v

    Npm test:

        npm -v




## MangoDB (version: 3.4.2) ##

Download source: [MongoDB](https://www.mongodb.com/download-center#community)

### Windows ###

* Install  MongoDB

1. To find which version of Windows you are running, enter the following commands in the Command Prompt or Powershell:

        wmic os get caption

    and: 

        wmic os get osarchitecture

2. Download the latest stable release of MongoDB from the [MongoDB downloads page](https://www.mongodb.com/download-center#community). Ensure you download the correct version of MongoDB for your Windows system. The 64-bit versions of MongoDB do not work with 32-bit Windows.

3. In Windows Explorer, locate the downloaded MongoDB .msi file, which typically is located in the default Downloads folder. Double-click the .msi file. A set of screens will appear to guide you through the installation process.

    > Note:  
You may specify an installation directory if you choose the “Custom” installation option.  
These instructions assume that you have installed MongoDB to ''C:\Program Files\MongoDB\Server\3.*\''.  
MongoDB is self-contained and does not have any other system dependencies. You can run MongoDB from any folder you choose. You may install MongoDB in any folder (e.g. D:\test\mongodb).  

* Set up the MongoDB environment

    >Note:  
MongoDB requires a data directory to store all data. MongoDB’s default data directory path is the absolute path \data\db on the drive from which you start MongoDB.

1. Create this folder by running the following command in a Command Prompt:

        md \data\db

    in PowerShell:

        mkdir /data/db

    > Note: this data directory will store all your mongoDB instances no matter which project you are working on, so create it at a more general place

    You can specify an alternate path for data files using the --dbpath option to mongod.exe, for example:

        "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --dbpath d:\test\mongodb\data  

    If your path includes spaces, enclose the entire path in double quotes, for example:

        "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --dbpath "d:\test\mongo db data"

    You may also specify the dbpath in a [configuration file](https://docs.mongodb.com/manual/reference/configuration-options/).  

* ### Start MongoDB. ###

    To start MongoDB, run mongod.exe. For example, from the Command Prompt:

        "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"

    This starts the main MongoDB database process. The waiting for connections message in the console output indicates that the mongod.exe process is running successfully.

    > Note:  
Depending on the security level of your system, Windows may pop up a Security Alert dialog box about blocking “some features” of C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe from communicating on networks. All users should select Private Networks, such as my home or work network and click Allow access. For additional information on security and MongoDB, please see the [Security Documentation](https://docs.mongodb.com/manual/security/).  

    .

    > Note:  
It's highly recommend you guys add mongoDB into windows service and turn the mongoDB service to auto start mode.

* ### Connect to MongoDB. ###

    To connect to MongoDB through the mongo.exe shell, open another Command Prompt.

        "C:\Program Files\MongoDB\Server\3.4\bin\mongo.exe





## mean.js ##

Once you cloned the repo "test_repo_1" to your localstream.(the copy of code in your local machine, we call it localstream, the code hosted by bitbucket in remote origin master branch, we call it upstream)

* ### Install Bower ###

    You're going to use the Bower Package Manager to manage your front-end packages, in order to install it make sure you've installed Node.js and npm, then install bower globally using npm:  

        npm install -g bower  


* ### Install Grunt ###

    You're going to use the Grunt Task Runner to automate your development process, in order to install it make sure you've installed Node.js and npm, then install grunt globally using npm:  

        npm install -g grunt-cli  

## Run the Code ##

Open a Command Prompt(or PowerShell, Git_bash, Terminal), go to the project folder and type:  

    npm install

> Note:  
this will download all the missing dependencies you need for running this project, the dependency information is stored in Package.json file in the project folder.

After the install process is over, you'll be able to run your application using Grunt, just run grunt default task:

    grunt